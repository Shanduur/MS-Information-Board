/*
 * MS-16.cpp
 *
 * Created: 1/7/2020 3:36:48 PM
 * Author : Mateusz Urbanek
 *
 */ 

#define F_CPU 16000000UL // 16MHz
#define USART_BAUDRATE 9600
#define BAUD_PRESCALE ((( F_CPU / ( USART_BAUDRATE * 16UL))) - 1)

#include <avr/io.h>
#include <util/delay.h>
#include <string.h>

extern "C" {

	#include "hd44780.h"

}

int pos = 0;
char str[16];

// transmit a char to uart
void uart_transmit( unsigned char data )
{
	// wait for empty transmit buffer
	while ( ! ( UCSR1A & ( 1 << UDRE1 ) ) );
	
	// put data into buffer, sends data
	UDR1 = data;
}

// read a char from uart
unsigned char uart_receive(void)
{
	while (!( UCSR1A & ( 1 << RXC1) ));
	
	return UDR1;
}

// init uart
void uart_init(void)
{
	// set baud rate
	unsigned int baud = BAUD_PRESCALE;
	
	UBRR1H = (unsigned char) (baud >> 8 );
	UBRR1L = (unsigned char)baud;
	
	// enable received and transmitter
	UCSR1B = ( 1 << RXEN1 ) | ( 1 << TXEN1 );
	
	// set frame format ( 8data, 2stop )
	UCSR1C = ( 1 << USBS1 ) | ( 3 << UCSZ10 );
}

// check if there are any chars to be read
int uart_dataAvailable(void)
{
	if ( UCSR1A & ( 1 << RXC1) )
	return 1;
	
	return 0;
}

// write a string to the uart
void uart_print( char data[] )
{
	int c = 0;
	
	for ( c = 0; c < strlen(data); c++ ) {
		uart_transmit(data[c]);
	}
}

// Replace all chars in string with ASCII 32 (space)
void clearStr(char c[16]) {
	for (int i = 0; i < 16; i++) {
		c[i] = 32;
	}
}

// Print CharTable on Screen
void putStr(char c[16]) {
	for (int i = 0; i < 16; i++) {
		lcd_putc(c[i]);
	}
}

// Move string to left if exceeding max length
void movStr(char c[16]) {
	for (int i = 0; i < 15; i++) {
		c[i] = c[i+1];
	}
	lcd_goto(0);
	putStr(c);
}

int main(void)
{
	// setup uart
	uart_init();
	// setup HD44780
	lcd_init();
	// clear display from any artifacts
	lcd_clrscr();
	
	uint8_t pos = 0;
	unsigned char receivedChar = '0';
	
	while (1)
	{
		if ( uart_dataAvailable() ) {
			char c = uart_receive();
			uart_transmit(receivedChar);
			lcd_goto(pos);
			
			if (c == 10 || c == 13) {
				lcd_goto(0);
				putStr(str);
				pos = 0;
				clearStr(str);
				lcd_goto(pos+15);
				putStr(str);
				} else {
				str[pos] = c;
				lcd_putc(c);
				pos++;
				if (pos >= 16) {
					pos = 15;
					movStr(str);
				}
			}
		}
	}
	
	return 0;
}
