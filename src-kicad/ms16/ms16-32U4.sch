EESchema Schematic File Version 4
LIBS:ms16-32U4-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Information Board"
Date "2020-01-06"
Rev "0.1"
Comp "Mateusz Urbanek"
Comment1 ""
Comment2 "in the computer via USART should be displayed on the LCD."
Comment3 "Information board - The string of characters specified from the terminal"
Comment4 "Project for Microprocessor Systems (5th semester on Silesian University of Technology)."
$EndDescr
$Comp
L Connector:Conn_01x16_Female J1
U 1 1 5E155BD7
P 9150 3150
F 0 "J1" H 9178 3126 50  0000 L CNN
F 1 "Display" H 9178 3035 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 9150 3150 50  0001 C CNN
F 3 "~" H 9150 3150 50  0001 C CNN
	1    9150 3150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0101
U 1 1 5E1588A6
P 8950 2550
F 0 "#PWR0101" H 8950 2400 50  0001 C CNN
F 1 "+5V" H 8965 2723 50  0000 C CNN
F 2 "" H 8950 2550 50  0001 C CNN
F 3 "" H 8950 2550 50  0001 C CNN
	1    8950 2550
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E158EE0
P 8950 2450
F 0 "#PWR0102" H 8950 2200 50  0001 C CNN
F 1 "GND" H 8950 2300 50  0000 R CNN
F 2 "" H 8950 2450 50  0001 C CNN
F 3 "" H 8950 2450 50  0001 C CNN
	1    8950 2450
	-1   0    0    1   
$EndComp
Text GLabel 8950 2750 0    50   Input ~ 0
RS
Text GLabel 8950 2950 0    50   Input ~ 0
EN
Text GLabel 8950 3450 0    50   Input ~ 0
D4
Text GLabel 8950 3550 0    50   Input ~ 0
D5
Text GLabel 8950 3650 0    50   Input ~ 0
D6
Text GLabel 8950 3750 0    50   Input ~ 0
D7
$Comp
L power:GND #PWR0105
U 1 1 5E19F476
P 8950 3950
F 0 "#PWR0105" H 8950 3700 50  0001 C CNN
F 1 "GND" H 8955 3777 50  0000 C CNN
F 2 "" H 8950 3950 50  0001 C CNN
F 3 "" H 8950 3950 50  0001 C CNN
	1    8950 3950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5E19FF55
P 8650 3850
F 0 "R3" V 8550 3850 50  0000 C CNN
F 1 "330R" V 8650 3850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P7.62mm_Horizontal" V 8580 3850 50  0001 C CNN
F 3 "~" H 8650 3850 50  0001 C CNN
	1    8650 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	8800 3850 8950 3850
$Comp
L power:+5V #PWR0106
U 1 1 5E1A281B
P 8450 3800
F 0 "#PWR0106" H 8450 3650 50  0001 C CNN
F 1 "+5V" H 8465 3973 50  0000 C CNN
F 2 "" H 8450 3800 50  0001 C CNN
F 3 "" H 8450 3800 50  0001 C CNN
	1    8450 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 3850 8450 3850
Wire Wire Line
	8450 3850 8450 3800
$Comp
L Device:R_POT RV1
U 1 1 5E1A92A4
P 8450 2650
F 0 "RV1" H 8300 2650 50  0000 C CNN
F 1 "50k" V 8450 2700 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 8450 2650 50  0001 C CNN
F 3 "~" H 8450 2650 50  0001 C CNN
	1    8450 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 2650 8950 2650
$Comp
L power:+5V #PWR0107
U 1 1 5E1B0699
P 8450 2500
F 0 "#PWR0107" H 8450 2350 50  0001 C CNN
F 1 "+5V" H 8465 2673 50  0000 C CNN
F 2 "" H 8450 2500 50  0001 C CNN
F 3 "" H 8450 2500 50  0001 C CNN
	1    8450 2500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 5E1B0FBF
P 8450 2800
F 0 "#PWR0108" H 8450 2550 50  0001 C CNN
F 1 "GND" H 8455 2627 50  0000 C CNN
F 2 "" H 8450 2800 50  0001 C CNN
F 3 "" H 8450 2800 50  0001 C CNN
	1    8450 2800
	1    0    0    -1  
$EndComp
Wire Notes Line
	9500 4250 8000 4250
Wire Notes Line
	8000 4250 8000 2000
Wire Notes Line
	8000 2000 9500 2000
Wire Notes Line
	9500 2000 9500 4250
Text Notes 8050 4200 0    50   ~ 0
Display Connector\n
$Comp
L promicro:ProMicro U1
U 1 1 5E14A14D
P 5700 3250
F 0 "U1" H 5700 4287 60  0000 C CNN
F 1 "ProMicro" H 5700 4181 60  0000 C CNN
F 2 "ext:ProMicro_v2_back" H 5800 2200 60  0001 C CNN
F 3 "" H 5800 2200 60  0000 C CNN
	1    5700 3250
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0109
U 1 1 5E14B056
P 6400 2800
F 0 "#PWR0109" H 6400 2650 50  0001 C CNN
F 1 "+5V" V 6415 2928 50  0000 L CNN
F 2 "" H 6400 2800 50  0001 C CNN
F 3 "" H 6400 2800 50  0001 C CNN
	1    6400 2800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5E14C040
P 4850 2850
F 0 "#PWR0110" H 4850 2600 50  0001 C CNN
F 1 "GND" H 4855 2677 50  0000 C CNN
F 2 "" H 4850 2850 50  0001 C CNN
F 3 "" H 4850 2850 50  0001 C CNN
	1    4850 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2800 4950 2800
Wire Wire Line
	5000 2700 4950 2700
Wire Wire Line
	4950 2700 4950 2800
Connection ~ 4950 2800
$Comp
L power:GND #PWR0111
U 1 1 5E14CECF
P 6400 2600
F 0 "#PWR0111" H 6400 2350 50  0001 C CNN
F 1 "GND" V 6405 2472 50  0000 R CNN
F 2 "" H 6400 2600 50  0001 C CNN
F 3 "" H 6400 2600 50  0001 C CNN
	1    6400 2600
	0    -1   -1   0   
$EndComp
Text GLabel 5000 2500 0    50   Input ~ 0
TX
Text GLabel 5000 2600 0    50   Input ~ 0
RX
NoConn ~ 8950 3050
NoConn ~ 8950 3150
NoConn ~ 8950 3250
NoConn ~ 8950 3350
NoConn ~ 6400 2700
NoConn ~ 6400 2500
NoConn ~ 6400 2900
NoConn ~ 6400 3000
NoConn ~ 6400 3100
NoConn ~ 6400 3200
NoConn ~ 5000 2900
NoConn ~ 5000 3000
NoConn ~ 5000 3100
Text GLabel 6400 3300 2    50   Input ~ 0
D7
Text GLabel 5000 3400 0    50   Input ~ 0
D6
Text GLabel 5000 3200 0    50   Input ~ 0
D5
Text GLabel 5000 3300 0    50   Input ~ 0
D4
Text GLabel 6400 3400 2    50   Input ~ 0
RS
Text GLabel 6400 3500 2    50   Input ~ 0
EN
Wire Wire Line
	4850 2800 4950 2800
Wire Wire Line
	4850 2850 4850 2800
$Comp
L power:GND #PWR0112
U 1 1 5E1C3B24
P 8950 2850
F 0 "#PWR0112" H 8950 2600 50  0001 C CNN
F 1 "GND" V 8955 2722 50  0000 R CNN
F 2 "" H 8950 2850 50  0001 C CNN
F 3 "" H 8950 2850 50  0001 C CNN
	1    8950 2850
	0    1    1    0   
$EndComp
NoConn ~ 5000 3500
NoConn ~ 5000 3600
NoConn ~ 6400 3600
$Comp
L Connector:Conn_01x06_Female J3
U 1 1 5E1713A4
P 3100 2450
F 0 "J3" H 3128 2426 50  0000 L CNN
F 1 "FTDI232" H 3128 2335 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 3100 2450 50  0001 C CNN
F 3 "~" H 3100 2450 50  0001 C CNN
	1    3100 2450
	1    0    0    -1  
$EndComp
Text GLabel 2900 2450 0    50   Input ~ 0
RX
Text GLabel 2900 2350 0    50   Input ~ 0
TX
Wire Notes Line
	3500 3100 2150 3100
Wire Notes Line
	2150 3100 2150 2000
Wire Notes Line
	2150 2000 3500 2000
Wire Notes Line
	3500 2000 3500 3100
Text Notes 2200 2100 0    50   ~ 0
FTDI 232 Connector
NoConn ~ 2900 2250
NoConn ~ 2900 2550
NoConn ~ 2900 2650
NoConn ~ 2900 2750
Wire Notes Line
	4500 4000 4500 2000
Wire Notes Line
	4500 2000 7000 2000
Wire Notes Line
	7000 2000 7000 4000
Wire Notes Line
	4500 4000 7000 4000
Text Notes 4550 3950 0    50   ~ 0
Arduino Pro Micro\n
$EndSCHEMATC
