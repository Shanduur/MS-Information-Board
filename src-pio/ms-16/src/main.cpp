#include <Arduino.h>
/*
 * MS-16.cpp
 *
 * Created: 1/7/2020 3:36:48 PM
 * Author : Mateusz Urbanek
 *
 */ 

#include <LiquidCrystal.h>

// Pin Definitions
#define LCD_PIN_RS  15
#define LCD_PIN_E 14
#define LCD_PIN_DB4 9
#define LCD_PIN_DB5 8
#define LCD_PIN_DB6 10
#define LCD_PIN_DB7 16

LiquidCrystal lcd(LCD_PIN_RS,LCD_PIN_E,LCD_PIN_DB4,LCD_PIN_DB5,LCD_PIN_DB6,LCD_PIN_DB7);

int pos = 0;
char str[16];

// Replace all chars in string with ASCII 32 (space)
void clearStr(char c[16]) {
  for (int i = 0; i < 16; i++) {
    c[i] = 32;
  }
}

// Print CharTable on Screen
void putStr(char c[16]) {
  for (int i = 0; i < 16; i++) {
    lcd.print(c[i]);
  }
}

// Move string to left if exceeding max length
void movStr(char c[16]) {
  for (int i = 0; i < 15; i++) {
    c[i] = c[i+1];
  }
  lcd.setCursor(0,1);
  putStr(c);
}

void setup() 
{
  // Setup Serial interface for communication
  Serial1.begin(9600);
  // set up the LCD's number of columns and rows
  lcd.begin(16, 2);
  lcd.display();
  clearStr(str);
}

// Main logic of your circuit.
void loop() 
{
  if (Serial1.available() > 0) {
    char c = Serial1.read();
    Serial1.write(c); // Echo the incomming message
    
    lcd.setCursor(pos, 1);
    if (c == 10 || c == 13) {
      lcd.setCursor(0,0);
      putStr(str);
      pos = 0;
      clearStr(str);
      lcd.setCursor(pos, 1);
      putStr(str);
    } else {
      str[pos] = c;
      lcd.print(c);
      pos++;
      if (pos >= 16) {
        pos = 15;
        movStr(str);
      }
    }
  }
}
